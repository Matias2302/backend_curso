var express = require('express');
var CardModel = require('../model/CardModel');
var router = express.Router();

router.get("/", function (req, res) {
    CardModel.find().then(function (cards) {
        res.json(cards)
    })
})

router.get('/:id', function (req, resp) {
    CardModel.findById(req.params.id).then(function (card) {
        resp.json(card);
    })
})

router.post('/', function (req, resp) {
    var card = new CardModel({
        name: req.body.name,
        img_url: req.body.img_url
    });
    card.save().then(function (card) {
        resp.json(card);
    })
})

router.delete('/:id', function (req, resp) {
    CardModel.deleteOne({'_id': req.params.id}).then(function () {
        resp.json()
    })
});

router.put('/:id', function (req, resp) {
    CardModel.findById(req.params.id).then(function (card) {
        card.name = req.body.name;
        card.img_url = req.body.img_url;
        card.save().then(function (card) {
            resp.json(card);
        })
    })
})

router.patch('/:id', function (req, resp) {
    CardModel.findById(req.params.id).then(function (card) {
        card.name = req.body.name ? req.body.name : card.name;
        card.img_url = req.body.img_url ? req.body.img_url : card.name;
        card.save().then(function (card) {
            resp.json(card);
        })
    })
})

module.exports = router;
