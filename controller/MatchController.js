var express = require('express');
var MatchModel = require('../model/MatchModel');
var router = express.Router();

router.get("/", function (req, res) {
    MatchModel.find().then(function (match) {
        res.json(match)
    })
})

router.get('/:id', function (req, resp) {
    MatchModel.findById(req.params.id).populate("player").then(function (match) {
        resp.json(match);
    })
})

router.post('/', function (req, resp) {
    var match = new MatchModel({
        name: req.body.name,
        player: req.body.playerID,

    });
    console.log(match);
    match.save().then(function (match) {
        console.log(match);
        resp.json(match);
    })
})

router.delete('/:id', function (req, resp) {
    MatchModel.deleteOne({'_id': req.params.id}).then(function () {
        resp.json()
    })
});

router.put('/:id', function (req, resp) {
    MatchModel.findById(req.params.id).then(function (match) {
        match.name = req.body.name;
        match.player = req.body.player;
        match.save().then(function (match) {
            resp.json(match);
        })
    })
})

router.patch('/:id', function (req, resp) {
    MatchModel.findById(req.params.id).then(function (match) {
        match.player = req.body.player ? req.body.player : match.player;
        match.save().then(function (match) {
            resp.json(match);
        })
    })
})

module.exports = router;
