var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/nerdos');

var UserSchema = new mongoose.Schema({
    name: String,
    email: String,
    password: String
})

var UserModel = mongoose.model('User', UserSchema);

module.exports = {UserModel, UserSchema};
